from flask import Flask
from flask_restful import Api, Resource, reqparse
from store import slangPanameno
import random

app = Flask(__name__)
api = Api(app)

class Slang(Resource):
     def get(self, id=0):
         if id == 0:
              return slangPanameno, 200

         for slang in slangPanameno:
              if(slang["id"] == id):
                   return slang, 200
         return "No hay resultado", 404

     def post(self, id):
        parser = reqparse.RequestParser()
        parser.add_argument("slang")
        parser.add_argument("definicion")
        parser.add_argument("ejemplo")
        params = parser.parse_args()

        for slang in slangPanameno:
            if(id == slang["id"]):
                return f"El slang con {id} ya existe", 400

        slang = {
            "id": int(id),
            "slang": params["slang"],
            "definicion": params["definicion"],
            "ejemplo": params["ejemplo"]
        }

        slangPanameno.append(slang)
        return slang, 201

     def put(self, id):
        parser = reqparse.RequestParser()
        parser.add_argument("slang")
        parser.add_argument("definicion")
        parser.add_argument("ejemplo")
        params = parser.parse_args()

        for slang in slangPanameno:
            if(id == slang["id"]):
                slang["slang"] = params["slang"]
                slang["definicion"] = params["definicion"]
                slang["ejemplo"] = params["ejemplo"]
                return slang, 200
        
        slang = {
            "id": id,
            "slang": params["slang"],
            "definicion": params["definicion"],
            "ejemplo": params["ejemplo"]
        }
        
        slangPanameno.append(slang)
        return slang, 201

     def delete(self, id):
      global slangPanameno
      slangPanameno = [slang for slang in slangPanameno if slang["id"] != id]
      return f"El slang {id} ha sido borrado", 200

api.add_resource(Slang, "/slang", "/slang/", "/slang/<int:id>")

if __name__ == '__main__':
    app.run(debug=True)