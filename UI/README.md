# Como visualizar la vista previa de ui

## es un html con su carpeta de estilo por lo cual necesitas

1. Descarga él [slangPA.html](https://gitlab.com/RichardQuiros/slang-panameno-api/-/blob/master/UI/slangPA.html)

2. Descarga la carpeta que contiene los estilos [slangPA_files](https://gitlab.com/RichardQuiros/slang-panameno-api/-/tree/master/UI/slangPA_files)

3. Asegúrate que los dos estén en el mismo directorio

4. Ejecuta le html y todo listo 🎉🎊