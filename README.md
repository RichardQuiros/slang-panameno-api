# Slang Panameño API 🇵🇦 Que xopa! <img src="https://raw.githubusercontent.com/MartinHeinz/MartinHeinz/master/wave.gif" width="30px">

> 🔸 versión temprana y en desarrollo

## ¿Qué es esto?

### Este es un proyecto de código abierto que lleva a cabo la conversión del slang panameño a una API en donde cualquiera persona pude hacer sus aportaciones de palabras. Lo puedes ver [aquí!](https://slangpanameno.herokuapp.com/slang)

# Uso
>Puedes usar [Postman](https://www.postman.com/) o cualquier testing de APIS para hacer la acciones de post put delete
>>![PostMAn](http://drive.google.com/uc?export=view&id=1uuRJokAIKEf5tjnN3tGIT_HXji2uLvgq)

## Ejemplo

- ### obtener datos

[https://slangpanameno.herokuapp.com/slang/20](https://slangpanameno.herokuapp.com/slang/20)

```
{
 "id": 20,
 "slang": "rantan",
 "definicion": "Cuando queremos decir que hay mucho de algo utilizamos la palabra rantan.",
 "ejemplo": "ratan gente está molesta con el presi"
}
```
- ### post datos
>para poder hacer un post primero debes posicionarte en un nuevo id y agregar las llaves y valor de slang,
>definicion, ejemplo en el header

```https://slangpanameno.herokuapp.com/slang/100?slang=NuevoSlang&definicion='definicion del slang'&ejemplo='ejemplo del slang```

- ### put datos
>para poder hacer un put primero debes posicionarte en el, id que quieras actualizar y agregar las llaves y valor de slang,
>definicion, ejemplo en el header

```https://slangpanameno.herokuapp.com/slang/100?slang=changedSlang&definicion='changed definicion'&ejemplo='changed ejemplo```

- ### delete datos
>para poder hacer un delete primero debes posicionarte en el, id que quieras borrar

```https://slangpanameno.herokuapp.com/slang/100```

>Puedes usar el CLI para hacer el crud además almacenar los datos localmente
>>[CLI slang panameño](https://gitlab.com/RichardQuiros/slang-panameno-cli)

>También se tiene pensado sacar una versión ui en una web app
>>[ver ui preview **concept**](https://gitlab.com/RichardQuiros/slang-panameno-api/-/tree/master/UI)

# 💢 Inconvenientes
#### ‣ La API actualmente no dispone de un servidor estable y esta hostiado por la versión gratuita de desarrollador de [HEROKU](https://dashboard.heroku.com)
#### ‣ Falta pulir y mejorar la API como ejemplo añadir COORS, organización de los datos o hacer que reciba request with parameters body.
#### ‣ Falta cumplir los requerimientos anteriores para comenzar con la ui
# Por ultimo decier que es un proyecto totalmente open source 🎊🎉
##### Puedes encontrarme en instagram como [@rquiros7255](https://www.instagram.com/quiros7255/?hl=en) <img src="https://cdn4.iconfinder.com/data/icons/materia-social-free/24/038_011_instagram_mobile_photo_network_android_material-128.png" width="20"/>

@RichardQuiros
