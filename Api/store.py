##PrimerStore
slangPanameno = [
    { 
        "id": 0,
        "slang": "",
        "definicion": "",
        "ejemplo":"" 
    },
    {
        "id": 1,
        "slang": "mopri",
        "definicion": "Tomamos la palabra primo y la convertimos en mopri, que no solo se refiere al hijo del tío de una persona como dice la RAE, sino que también describe a un amigo o pasiero.",
        "ejemplo":"Hey mopri vamos correr"
    },
    {
        "id": 2,
        "slang": "ahuevao/awebao",
        "definicion": "Persona que comprende o capta las cosas más lento que el promedio o que entiende una cosa distinta a lo que el resto del grupo comprendió.",
        "ejemplo":"tas awebao,Como no vas a saber que era suyo"
    },
    {
        "id": 3,
        "slang": "llesca",
        "definicion": "Otro caso en el que usamos la inversión de sílabas es al hablar de la llesca, en lugar de la calle. ",
        "ejemplo":"la llesca esta dura:'La situacion económica está complicada'"
    },
    {
        "id": 4,
        "slang": "chuzo",
        "definicion": "Es una interjección que utilizamos para verbalizar algún sentimiento o impresión. Por ejemplo, si acabas de recordar algo que tenías que hacer y no hiciste dices «¡chuzo!, se me pasó ir al cajero»; si te preguntan sobre algo que pasó hace rato y ya casi no recuerdas, «¡chuzo!, déjame pensar»; si alguna situación movió algo en tu memoria, «¡chuzo!, qué buenos recuerdos». También decimos «¡váyala vida!» o «¡váyala peste!» y con el mismo significado aunque ya en desuso tenemos la expresión «¡chuleta!».",
        "ejemplo":"chuzo se me quedo la tarea" 
    },
    {
        "id": 5,
        "slang": "compa",
        "definicion": "Es una abreviación de la palabra compadre y se utiliza, especialmente entre hombres, bien para referirse a un amigo o entre desconocidos pero buscando dar un trato afable y cortés a la vez.",
        "ejemplo":"¿compa y ya tienes el dinero que me debes?"
    },
    {
        "id": 6,
        "slang": "Chambón",
        "definicion": "Un chambón es una persona que no tiene muchas habilidades para desempeñarse en alguna actividad que puede ser deportiva.",
        "ejemplo":"ese tipo es bien chambón como electricista"
    },
    {
        "id": 7,
        "slang": "birria",
        "definicion": "Jugar.",
        "ejemplo":"vamos a birriar FreeFire"
    },
    {
        "id": 8,
        "slang": "bulto",
        "definicion": "Un bulto es una persona que es mala, realmente mala en lo que se desempeña.",
        "ejemplo":"ese man es un bulto como representante sindical"
    },
    {
        "id": 9,
        "slang": "Chachai",
        "definicion": "Cuando a las niñas les ponen un traje de esos lindos, con vuelo y lazo atrás la gente suele decirles «¡qué lindo tu chachai», pero también entre adultos se puede usar esta expresión para referirse a que la ropa que lleva puesta está bonita o le queda bien.",
        "ejemplo":"¡qué lindo tu chachai" 
    },
    {
        "id": 10,
        "slang": "chaniao/chaneao/chaneado",
        "definicion": "Cuando una persona se arregla o se viste mejor de lo habitual se le dice que está chaniao. Esta palabra proviene del inglés shining.",
        "ejemplo":"Ese mas esta chaneado para la fiesta"
    },
    {
        "id": 11,
        "slang": "tongo",
        "definicion": "A los agentes de la fuerza del orden se les llama tongos aquí en Panamá. Generalmente nos referimos así a los rasos o que no tienen rango.",
        "ejemplo":"vienen los togos!"
    },
    {
        "id": 12,
        "slang": "guabanazo",
        "definicion": "Cuando una persona se da un buen golpe decimos que se dio un guabanazo. Suele tratarse de un golpe accidental, aunque una persona también le puede dar un guabanazo a otra.",
        "ejemplo":"que guabanazo se dio ese tipo cuando se cayó  de la bicicleta"
    },
    {
        "id": 13,
        "slang": "golpe de ala",
        "definicion": "Simplemente mal olor en el sobaco.",
        "ejemplo":"sentiste el golpe de ala cuando paso esa gial"
    },
    {
        "id": 14,
        "slang": "Biencuidao",
        "definicion": "En muchas partes del mundo se ha hecho común encontrar personas que por una propina te cuidan el carro mientras lo dejas estacionado y aquí en Panamá nos hemos dado a bien llamar a estar personas biencuidao.", 
        "ejemplo":"los biencuidao abundan por aquí"
    },
    {
        "id": 15,
        "slang": "chacalita/rakataca",
        "definicion": "Estos dos términos se emplean para referirse a chicas o mujeres, generalmente de barrios pobres o del gueto, que actúan y hablan como maleantes no siéndolo necesariamente. Los hombres también pueden ser chacalitos.", 
        "ejemplo":"esa chacalita es bastante grosera pero es buena persona"
    },
    {
        "id": 16,
        "slang": "que sopa",
        "definicion": "Que paso.", 
        "ejemplo":"Que sopa con tu amigo anda raro"
    },
    {
        "id": 17,
        "slang": "ñamería",
        "definicion": "Para hacer referencia a una persona que está loca, a veces utilizamos el apelativo ñame y si se trata de las locuras que hace o dice esta persona, pues hablamos de ñamerías.", 
        "ejemplo":"puras ñamerias habla ese profesor"
    },
    {
        "id": 18,
        "slang": "ñecks",
        "definicion": "Si una persona se saca (o le sacan) la ñecks es porque tuvo un accidente muy grave, una caída aparatosa o alguien le dio una paliza.", 
        "ejemplo":"a ese tipo la mujer le saco la ñecks por no hacerle caso"
    },
    {
        "id": 19,
        "slang": "taquillar/takillar",
        "definicion": "Cuando una persona busca ganar popularidad y aceptación, está taquillando, cuando quiere llamar la atención o lucirse ante los demás, está taquillando. Dicho de otro modo, si una persona pifea sus pertenencias o sus actos delante de los demás o a través de las redes sociales definitivamente es un taquillero.", 
        "ejemplo":"ese man viene a taquillar sus zapatos"
    },
    {
        "id": 20,
        "slang": "rantan",
        "definicion": "Cuando queremos decir que hay mucho de algo utilizamos la palabra rantan.",
        "ejemplo":"ratan gente esta molesta con el presi"
    }
]